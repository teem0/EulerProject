package Euler;

public class Euler10 {
	/* contrary to Euler7 in which used huge linked list of Primes
	 *  I am using very few variables in this problem
	 *  its taking more than 2 mins to get 2 mils.
	 *  initial prime number is 2, which is the only even number prime. 
	 *  other than 2, all primes will odd number.
	 *  it is a little bit more efficient to increase counters by 2, but not 1.	
	 */
	static int limit = 2000000;
	static int curPrime = 3;
	static long result = 2;

	public static void main(String arg[]){
		
		while(curPrime <= limit){
			result = result + curPrime; // increase result by current prime number
			curPrime = curPrime +2;		// increase current prime by 2, for odd# + 2 = odd#
			assign_Next_Prime(curPrime);// Find next prime #
		}
		System.out.println(result);
	}

	public static void assign_Next_Prime(int curPrime1) {
		
			int loc_count = 3;
			boolean prime = false;
			
			while(!prime){
				
				while(loc_count <= curPrime1){
					if((curPrime1%loc_count) == 0 && loc_count == curPrime1){
						// if loc_count is the only number that can divide curPrime1, then it is a prime number, so!
						prime = true;
						break;
					}else if((curPrime1%loc_count) == 0 && loc_count < curPrime1){
						//or if we can devide curprime by loc_count but less than curPrime, it means that
						//curprime is not prime number
						break;
					}else{
						//increase loc_count by 2 if no prime found.
						loc_count = loc_count +2;
					}
					
				}
				if(!prime){
					//if curprime1 is turned to be not a prime #, 
					//reset loc_count and increase curPrime by 2
					loc_count = 3;
					curPrime1 = curPrime1 +2;
				}
			}
			//we found hit.
			curPrime = curPrime1;
	}
}
