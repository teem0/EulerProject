package Euler;

public class Euler6 {
	static int count = 100;
	
	public static void main(String arg[]){
		System.out.println(sqr_sum()-sum_sqr());
		
	}
	public static int sum_sqr(){
		int buff = 0;
		while(count > 0){
			buff = buff + (count*count);
			count--;
		}
		count = 100;
		return buff;
	}
	public static int sqr_sum(){
		int buff =0;
		while(count > 0){
			buff = buff + count;
			count--;
		}
		buff = (buff*buff);
		count = 100;
		return buff;
	}
}