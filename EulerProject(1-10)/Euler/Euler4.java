package Euler;

public class Euler4 {
	static int reverse;
	static int target = 999*999;//largest # is 999*999
	public static void main(String[] args){
		int ans = 0;		  //ans checker
		
		while(ans==0){		
			palen();  // reverse holds palen number!
			ans = check(reverse);//find a largest 3 digit number that divides reverse
			if(ans != 0 && (reverse/ans) >100 && reverse/ans <1000){//if found and the other number is also greater than 100 and smaller than 1000, 
				System.out.println(ans);// we hit it
				System.out.println(reverse/ans); //second three digit number
				System.out.println(target); //print out palendrom #
			}else{
				ans = 0;//or, start from beginning
				target--; //with breaking existing palendrom #
			}
		}
	}
	public static int check(int i){
		int start = 999;	//from 999
		while(start > 100){ //until 100
			if(i%start ==0){ //if given number(i) is divisible by start,  
				return start;//return start, because it is 3 digit number
			}else{
				start--; //or decrease start by 1
			}
		}
		return 0; //if nothing is found, return 0
	}
	public static void palen(){
		int rev = 0;//reverse target
		int buff = target; //copy current target
		while(buff != 0){ //do reverse number
			rev = (rev*10) + (buff%10); 
			buff = buff/10;
		}
		if(rev-target ==0){// if palendrom, 
			reverse = rev;// return that, 
		}else{
			target--;	//or start again with decrement of target
			palen();
		}
		
	}
}