package Euler;

public class Euler5 {
	/*
	 * We have number from 1 to 20 in where
	 *        2 		   3  2			   2					4	   2          2
	 * 1 2 3 2  5 (2*3) 7 2	 3  (2*5) 11 (2 *3) 13 (2*7) (3*5) 2  17 (3 * 2) 19 (2  * 5)
	 * 
	 * So our minimum should be multiple of all the primes.
	 * we Also can remove some numbers sharing its GCF which is 4,6,8,9,10
	 * That leaves only12,14,15,16,18,20 to check to get the right number!
	 * 
	 */
	static int num_basic = 2*3*5*7; // those are prime..
	public static void main(String arg[]){
		boolean isit = true;
		while(isit){
			if(num_basic%12==0 && num_basic%11==0 &&
			   num_basic%14==0 && num_basic%13==0 &&
			   num_basic%15==0 && num_basic%17==0 &&
			   num_basic%16==0 && num_basic%19==0 &&
			   num_basic%18==0 && 
			   num_basic%20==0){
				System.out.println(num_basic);
				isit=false;
			}else{
				num_basic++;
			}
			
		}
	}
}
