package Euler;
import java.util.*;

public class Euler7 {
	static List<Integer> prime; //space to save primes
	static int count = 0;	//counter
	
	public static void main(String arg[]){
		prime = new ArrayList<Integer>(); 
		prime.add(2); //initial prime value is 2
		
		while(count != 10001){ //until we have 10001 primes, do
			nextPrime();		// find next prime #
			count++;			// count ++
		}
		System.out.println(prime.get(10000)); //print 10001th prime
	}
	
	public static void nextPrime(){	
		
		int loc_count = 0;	//set local count to 0
		
		int nextprime = prime.get(count) + 1;// get the currently largest prime # to find next prime number

		while(true){	//run till find one 
			
			while(loc_count != count){	//until we run out of prime number in Arraylist, 
				if(nextprime % prime.get(loc_count) ==0){	// if any prime can divide nextprime, 
					break;	//then, it is not a prime, so break!
				}
				loc_count++; // if not, divide with next element in arraylist
			}
			if(loc_count != count){	// if we have not used all the element, it means it is not a prime
				nextprime++;	// so we increase value of nextprime and 
				loc_count=0;	//set loc_count to 0 to continue.
			}else{				// or if we use all, 
				prime.add(nextprime);	// it is prime! so add to the array list and 
				break;					//break!
			}
		}
	}
}
