package Euler;

public class Euler3 {
	public static void main(String[] args){
		long target = 600851475143L;
		long ans = find_P(target);
		System.out.println(ans);
	}
	
	static int find_P(long target){
		long buf = target; 
		int ans=0;
		
		while(buf > ans){
			for(int i =2; i<=buf; i++){
				if(buf%i == 0){
					buf = buf/i;
					ans = i;
				}
			}
		}
		
			return ans;
	}
	
}
