package Euler;

public class Euler2 {
	public static void main(String[] args){
		int sum = 0;
		int buf =0;
		int f1 = 1;
		int f2 = 1;
		while(f1<4000000){
			buf = f2;
			f2 = f1+f2;
			f1 = buf;
			if(f1%2==0){
				sum=sum+f1;
			}
		}
		System.out.print(sum);
	}
}
