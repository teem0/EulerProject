package Euler;

public class Euler9 {
	static int a,b,c;
	
	public static void main(String[] arg){
		//initial value that a + b + c = 1000;
		//selected reasonable numbers
		// a < b < c
		a = 1;
		b = 2;
		c = 997;
		
		while(!pita()){	//if not pita triangle
			b++;		//increase b by 1
			c--;		//decrease c by 1 so that a + b + c  = 1000;
			if(b > c){  // if b >c, it violates (a < b < c) so need to reset
				a++;	// increase a by 1
				b=a+1;	// increase b by a + 1 to satisfy a<b
				c = 1000 - a - b;	// c needs to be the number subtracted by a and b to satisfy a+b+c = 0
			}
		}
		System.out.printf("%d,%d,%d\n", a,b,c);
		System.out.println((a*b*c)); //print out the result;
	}
	public static Boolean pita(){
		if((a*a) + (b*b) == (c*c)){
			return true;
		}else{ 
			return false;
		}
	}
}
