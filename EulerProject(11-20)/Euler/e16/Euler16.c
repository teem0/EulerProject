#include <stdio.h>
#include <math.h>
#include <time.h>
/*define 
 * MAX size of number
 * BASE
 * EXPONENT
 */
#define SIZE 1024
#define BASE 2
#define EXPONENT 1000

int main(){
	time_t start, end;
	start = clock();

	//make array
	char result[SIZE];
	//then calculate BASE^EXPONENT and put the result in the array
        snprintf(result, SIZE, "%f", pow(BASE,EXPONENT));
	//init counter and index	
	int count=0, index=0;
	//since the number is double, only need to add up to decimal point!
	while(result[index] != '.'){
		count += result[index] - '0';
		index++;
	}
	//here is the result
	printf("%d is final result\n",count);

	end = clock();
	printf("TOTAL TIME TAKEN : %f\n",(float)(end-start)/(CLOCKS_PER_SEC));
	return 0;
}
