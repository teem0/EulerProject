#include<stdio.h>
#define MAX 1000000

int count=1,cur=1;
int max=0, mp=0;

unsigned int odd(unsigned int target){
	return (3*target) +1;
}
unsigned int even(unsigned int target){
	return target/2;
}
int calc(int target){
	unsigned int temp=target;
	int count=1;
	while(temp > 1){
	    if(temp%2 ==0)
		temp=even(temp);
	    else
		temp=odd(temp);
	    count++;
	}
	return count;
}

int main(){
    while(cur<MAX){
	count = calc(cur);
	if(count > max){
	    max=count;
	    mp=cur; 
	    printf("Max has changed! to %d with count%d \n",mp,max);
	}
	cur++;
    }
    printf("MAX num is %d, and link %d ",mp,max);
}
