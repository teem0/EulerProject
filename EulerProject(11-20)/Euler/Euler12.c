#include <stdio.h> 

int largest, count;

int triangle(int i){ 
	return i*(i+1)/2;
}


int check(int largest){
	int tri = triangle(largest);
	int temp, buf;
	buf =0;
	for(temp=1; temp<=tri; temp++){
		if(tri%temp == 0){
			buf++;
		}
	}
	return buf;
}

int main(void){
	count = 0;
	largest =0;
	
	while(count<500){
		largest++;
		count = check(largest);
		printf("Current triangle : %d \t\t Current count on largest %d \n", triangle(largest), count);
	}
	printf("%d is the num of divisor of %d",count, largest);
	return 0;
}
