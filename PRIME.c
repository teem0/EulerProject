#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#define LIM 500

typedef struct _p_node{
	int prime;
	struct _p_node *next;
}prime;
/*create linked list for prime numbers*/

prime *p_list;
/*create global linked list*/

void init(prime *plist){
	int target;
	prime *cur = (prime*)malloc(sizeof(prime));

	plist->prime = 2;
	plist->next = cur;
	/*init first node! plist is the head node!*/

	cur->prime = 3;
	cur->next = NULL;
	/*init second node for I will not touch headnode*/

	target = cur->prime+1;
	/*to find next prime, val of target is 1 greater than cur->prime*/
	
	while(target < LIM){
	/*while val of current prime is less than LIM*/

		cur = plist;
		/*we want to scan from the beginning*/
		
		while(cur->next != NULL){
		/*while there is node remaining*/
		
			if(target%cur->prime ==0) break;
			/*if val of target can be divided by any prime, it is not prime, so break the loop*/
			
			else cur = cur->next;
			/*if it is not divisible, keep proceed.*/
		}
		if(cur->next != NULL){
			target++;
		} 
		/*if target is divisible, increase target by 1 and repeat.
		  NOTE : the cur->next should not be NULL and divisible at the same time.
		  		  All composite number is multiple of primes.*/
		else{
			prime *next_node = (prime*)malloc(sizeof(prime));
			next_node->prime = target;
			next_node->next = NULL;
			cur->next = next_node;
			target++;
		}
		/*if the target is not divisible by any of prime numbers in list,
		  make new node, and save val of target into the node and make it next node of current node*/
	}
}
int classify(int determine){
	prime *temp = p_list;
	/*we never move p_list because there is no way to come back to original head node if head node is moved*/
	int ret;
	while(temp->prime < determine){
		temp = temp->next;
	}/*move the node until prime is larger or equal to determine*/
	
	if(temp->prime == determine){
		printf("%d is indeed Prime!\n",determine);
	}else{
		printf("%d is not a PrImE number!\n",determine);
	}
	/*if found exact match, print yes, else no*/
	
	printf("wanna try again? Type 0 to exit, 1 to retry ");
	scanf("%d", &ret);
	assert(ret==0 || ret ==1);
	return ret;
}
void CLEAR(){
	while(p_list->next != NULL){
		prime *del = p_list;
		p_list = p_list->next;
		free(del);
	}
	free(p_list);
}/*freeing all the linked list created at the end of program*/


int main(){
	p_list = (prime*) malloc(sizeof(prime));
	init(p_list);
	
	int exit = 1;
	int determine;
	
	while(exit){
		printf("Please type number to classfy(1 < x < %d)", LIM);
		scanf("%d", &determine);
		assert(determine<=500);
		exit = classify(determine);
	}	
	
	
	CLEAR();
	return 0;
}


